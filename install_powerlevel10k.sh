#!/usr/bin/env bash
: <<COMMENTBLOCK
title				:Install Powerlevel10K on Oh My Zsh
description	:
author			:Magomed Gamadaev
email				:mag@strategic.zone
date				:18092019
version			:0.1
notes				:
================================================
COMMENTBLOCK


set -e

#check if commandes exists
if ! [ -x "$(command -v git)" ]; then
  echo 'Error: please install git.' >&2
  exit 1
fi
if ! [ -x "$(command -v zsh)" ]; then
	echo 'Error: please install zsh.' >&2
	exit 1
fi
if [ ! -d ~/.oh-my-zsh ]; then
  echo -e 'Error: please install oh_my_zsh:\nsh <(curl --silent https://gitlab.com/strategic.zone/install_ohmyzsh/raw/master/install_ohmyzsh.sh)' >&2
  exit 1
fi

# Creation config de zsh avec oh-my-zsh
git clone https://github.com/romkatv/powerlevel10k.git ~/.oh-my-zsh/custom/themes/powerlevel10k
if [ -f ~/.p10k.zsh ]; then
	cp ~/.p10k.zsh ~/.p10k.zsh.orig
fi
curl -s  https://gitlab.com/strategiczone/install_powerlevel10k/raw/master/.p10k.zsh -o ~/.p10k.zsh

if [ -f ~/.zshrc ]; then
	cp ~/.zshrc ~/.zshrc.orig
fi
sed -i 's/^ZSH_THEME.*$/ZSH_THEME="powerlevel10k\/powerlevel10k"/' ~/.zshrc

echo -e '\n# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.\n[[ -f ~/.p10k.zsh ]] && source ~/.p10k.zsh' >> ~/.zshrc
echo -e '\nInstallation completed successfully. To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.\n\nRecommended: Meslo Nerd Font patched for Powerlevel10k:\nhttps://github.com/romkatv/powerlevel10k#recommended-meslo-nerd-font-patched-for-powerlevel10k'
