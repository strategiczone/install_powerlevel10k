### Install Powerlevel10k

Install first Oh My Zsh
```
sh <(curl --silent https://gitlab.com/strategic.zone/install_ohmyzsh/raw/master/install_ohmyzsh.sh)
```

Then install Powerlevel10k
```
sh <(curl --silent https://gitlab.com/strategiczone/install_powerlevel10k/raw/master/install_powerlevel10k.sh)
```

![My p10k](./p10k.png)
